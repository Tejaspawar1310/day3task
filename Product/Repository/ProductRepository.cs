﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Products;
using Products.Model;

namespace Products.Repository
{
    internal class ProductRepository
    {
        Product[] products;
        
        public ProductRepository()
        {
            products = new Product[]
            {
                new Product("oppo","mobile",15000,4.5f),
                new Product("oneplus","mobile",25000,4.8f),
                new Product("samsung","tv",15000,3.7f),
                new Product("hp","laptop",55000,4.2f)
            };
         

        }
        public Product[] GetAllProducts()
        {
            return products;

        }

        public void UpdatedArray(int index , string name1, string category1, int price1, float rating1)
        {
            products[index].Name = name1;
            products[index].Category = category1;
            products[index].Price = price1;
            products[index].Rating = rating1;


        }
        public void DeleteProduct(int indexD)
        {
            int i = 0;
            foreach(Product p in products)
            {
                i++;
            }
            Product[] fproduct = new Product[i-1];
            int a = 0; 
            foreach(Product item1 in products)
            {
                if(item1!= products[indexD])
                {
                    fproduct[a] = item1;
                    a++;
                }
            }
            products = fproduct;
        }
    }
}
